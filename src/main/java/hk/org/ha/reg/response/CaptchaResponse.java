package hk.org.ha.reg.response;

import java.util.Date;
import java.util.List;

public class CaptchaResponse {

	private Boolean success;
	private Date challengeTs;
	private String hostname;
	private List<String> errorCodes;
	private String action;
	private Float score;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Date getChallengeTs() {
		return challengeTs;
	}

	public void setChallengeTs(Date challengeTs) {
		this.challengeTs = challengeTs;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public List<String> getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(List<String> errorCodes) {
		this.errorCodes = errorCodes;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "CaptchaResponse [success=" + success + ", challengeTs=" + challengeTs + ", hostname=" + hostname + ", errorCodes=" + errorCodes + ", action=" + action + ", score=" + score + "]";
	}

}