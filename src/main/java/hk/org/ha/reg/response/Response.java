package hk.org.ha.reg.response;

public class Response<T> {
    private Integer responseCode;
    private String errorMessage;
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Response() {
    }

    public Response(Integer responseCode, String errorMessage, T data) {
        this.responseCode = responseCode;
        this.errorMessage = errorMessage;
        this.data = data;
    }
    public static Response success() {
        Response response = new Response();
        response.setResponseCode(200);
        response.setErrorMessage("Success");
        return response;
    }
    public static Response success(Object data ) {
        Response response = new Response();
        response.setResponseCode(200);
        response.setErrorMessage("success");
        response.setData(data);
        return response;
    }
    public static Response result() {
        Response response = new Response();
        response.setResponseCode(201);
        response.setErrorMessage("fail");
        return response;
    }
    public static Response result(int responseCode, String errorMessage){
        Response response = new Response();
        response.setResponseCode(responseCode);
        response.setErrorMessage(errorMessage);
        return response;
    }
}
