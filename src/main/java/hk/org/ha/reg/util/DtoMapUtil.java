package hk.org.ha.reg.util;

import hk.org.ha.reg.pojo.dto.RegdbUserDto;
import hk.org.ha.reg.pojo.po.RegdbUserPo;
import hk.org.ha.reg.pojo.questionEnum.QuestionEnum;


public class DtoMapUtil {
    private DtoMapUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static RegdbUserPo convertRegdbUserPo(RegdbUserDto dto) {
        RegdbUserPo po = new RegdbUserPo();
        po.setUserName(dto.getUserName());
        po.setPhoneNumber(dto.getPhoneNumber());
        po.setEmail(dto.getEmail());
        po.setQuestion1(QuestionEnum.Question1Enum.getAnswerByIndex(dto.getQuestion1()));
        po.setQuestion2(QuestionEnum.Question2Enum.getAnswerByIndex(dto.getQuestion2()));
        po.setQuestion3(QuestionEnum.Question3Enum.getAnswerByIndex(dto.getQuestion3()));
        po.setQuestion4(QuestionEnum.Question4Enum.getAnswerByIndex(dto.getQuestion4()));
        po.setQuestion5(QuestionEnum.Question5Enum.getAnswerByIndex(dto.getQuestion5()));
        return po;
    }
}
