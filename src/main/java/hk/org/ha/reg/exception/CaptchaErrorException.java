package hk.org.ha.reg.exception;

public class CaptchaErrorException extends RegException {
	public CaptchaErrorException(String reason) {
		super(reason);
	}

}
