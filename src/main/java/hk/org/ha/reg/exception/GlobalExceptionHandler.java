package hk.org.ha.reg.exception;

import hk.org.ha.reg.response.Response;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public Response exceptionHandler(Exception e){
        return Response.result(203, "System error");
    }
}
