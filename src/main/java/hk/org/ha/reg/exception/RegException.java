package hk.org.ha.reg.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RegException extends ResponseStatusException {
	public RegException(String reason) {
		super(HttpStatus.BAD_REQUEST, reason);
	}

}
