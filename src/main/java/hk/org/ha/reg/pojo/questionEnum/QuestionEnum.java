package hk.org.ha.reg.pojo.questionEnum;

public class QuestionEnum {

    public enum Question1Enum{
        ANSWER1("Answer1", "Yes"),
        ANSWER2("Answer2", "No");
        private String index;
        private String answer;
        Question1Enum(String index, String answer ) {
            this.answer = answer;
            this.index = index;
        }
        public String getIndex() {
            return index;
        }
        public String getAnswer() {
            return answer;
        }

        public static String getAnswerByIndex(String index) {
            for (Question1Enum question1Enum : Question1Enum.values()) {
                if (question1Enum.getIndex().equals(index)) {
                    return question1Enum.answer;
                }
            }
            return null;
        }
    }
    public enum Question2Enum{
        ANSWER1("Answer1","Yes"),
        ANSWER2("Answer2","No");
        private String index;
        private String answer;
        Question2Enum(String index, String answer ) {
            this.answer = answer;
            this.index = index;
        }
        public String getIndex() {
            return index;
        }
        public String getAnswer() {
            return answer;
        }

        public static String getAnswerByIndex(String index) {
            for (Question2Enum question2Enum : Question2Enum.values()) {
                if (question2Enum.getIndex().equals(index)) {
                    return question2Enum.answer;
                }
            }
            return null;
        }
    }
    public enum Question3Enum{
        ANSWER1("Answer1", "Graduated from medical degree but have not yet completed internship training"),
        ANSWER2("Answer2", "Graduated from medical degree and completed internship training"),
        ANSWER3("Answer3", "Attained a qualification comparable to the Intermediate (or Pre-intermediate*) examination of the Hong Kong Academy of Medicine (examples)"),
        ANSWER4("Answer4", "Certified or registered as a specialist or equivalent, in relevant specialty in the country of practising Medicine"),
        ANSWER5("Answer5", "Attained specialist qualification awarded or recognized by the Hong Kong Academy of Medicine");

        private String index;
        private String answer;
        Question3Enum(String index, String answer ) {
            this.answer = answer;
            this.index = index;
        }
        public String getIndex() {
            return index;
        }
        public String getAnswer() {
            return answer;
        }

        public static String getAnswerByIndex(String index) {
            for (Question3Enum question3Enum : Question3Enum.values()) {
                if (question3Enum.getIndex().equals(index)) {
                    return question3Enum.answer;
                }
            }
            return null;
        }
    }
    public enum Question4Enum{
        ANSWER1("Answer1", "Anaesthesiology"),
        ANSWER2("Answer2", "Clinical Oncology"),
        ANSWER3("Answer3", "Clinical Oncology"),
        ANSWER4("Answer4", "Family Medicine"),
        ANSWER5("Answer5", "Intensive Care"),
        ANSWER6("Answer6", "Internal Medicine"),
        ANSWER7("Answer7", "Obstetrics & Gynaecology"),
        ANSWER8("Answer8", "Ophthalmology"),
        ANSWER9("Answer9", "Orthpaedics & Traumatology"),
        ANSWER10("Answer10", "Otorhinolaryngology"),
        ANSWER11("Answer11", "Paediatrics"),
        ANSWER12("Answer12", "Pathology"),
        ANSWER13("Answer13", "Psychiatry"),
        ANSWER14("Answer14", "Radiology"),
        ANSWER15("Answer15", "Nuclear Medicine"),
        ANSWER16("Answer16", "General Surgery"),
        ANSWER17("Answer17", "Cardiothoracic Surgery"),
        ANSWER18("Answer18", "Neurosurgery"),
        ANSWER19("Answer19", "Plastic Surgery");
        private String index;
        private String answer;
        Question4Enum(String index, String answer ) {
            this.answer = answer;
            this.index = index;
        }
        public String getIndex() {
            return index;
        }
        public String getAnswer() {
            return answer;
        }

        public static String getAnswerByIndex(String index) {
            for (Question4Enum question4Enum : Question4Enum.values()) {
                if (question4Enum.getIndex().equals(index)) {
                    return question4Enum.answer;
                }
            }
            return null;
        }
    }
    public enum Question5Enum{
        ANSWER1("Answer1", "HA website"),
        ANSWER2("Answer2", "Medical conference"),
        ANSWER3("Answer3", "Medical journal"),
        ANSWER4("Answer4", "Newspaper"),
        ANSWER5("Answer5", "On-line Media"),
        ANSWER6("Answer6", "Relatives & friends");

        private String index;
        private String answer;
        Question5Enum(String index, String answer ) {
            this.answer = answer;
            this.index = index;
        }
        public String getIndex() {
            return index;
        }
        public String getAnswer() {
            return answer;
        }

        public static String getAnswerByIndex(String index) {
            for (Question5Enum question5Enum : Question5Enum.values()) {
                if (question5Enum.getIndex().equals(index)) {
                    return question5Enum.answer;
                }
            }
            return null;
        }
    }
}
