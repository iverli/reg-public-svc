package hk.org.ha.reg.pojo.po;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "REGDB_USER")
public class RegdbUserPo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REGDB_USER_ID")
    private Integer regdbUserId;
    @Column(name = "NAME", nullable = false)
    private String userName;
    @Column(name = "PHONE_NUMBER", nullable = false)
    private String phoneNumber;
    @Column(name = "EMAIL", nullable = false)
    private String email;
    @Column(name = "QUESTION1", nullable = false)
    private String question1;
    @Column(name = "QUESTION2", nullable = false)
    private String question2;
    @Column(name = "QUESTION3", nullable = false)
    private String question3;
    @Column(name = "QUESTION4", nullable = false)
    private String question4;
    @Column(name = "QUESTION5", nullable = false)
    private String question5;
    @Column(name = "QUESTION6")
    private String question6;
    @Column(name = "QUESTION7")
    private String question7;
    @Column(name = "QUESTION8")
    private String question8;
    @Column(name = "QUESTION9")
    private String question9;
    @Column(name = "QUESTION10")
    private String question10;
    @Column(name = "SUBMIT_DATETIME")
    private Date submitDatetime;

    public RegdbUserPo() {
    }

    public Integer getRegdbUserId() {
        return regdbUserId;
    }

    public void setRegdbUserId(Integer regdbUserId) {
        this.regdbUserId = regdbUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    public String getQuestion3() {
        return question3;
    }

    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    public String getQuestion4() {
        return question4;
    }

    public void setQuestion4(String question4) {
        this.question4 = question4;
    }

    public String getQuestion5() {
        return question5;
    }

    public void setQuestion5(String question5) {
        this.question5 = question5;
    }

    public String getQuestion6() {
        return question6;
    }

    public void setQuestion6(String question6) {
        this.question6 = question6;
    }

    public String getQuestion7() {
        return question7;
    }

    public void setQuestion7(String question7) {
        this.question7 = question7;
    }

    public String getQuestion8() {
        return question8;
    }

    public void setQuestion8(String question8) {
        this.question8 = question8;
    }

    public String getQuestion9() {
        return question9;
    }

    public void setQuestion9(String question9) {
        this.question9 = question9;
    }

    public String getQuestion10() {
        return question10;
    }

    public void setQuestion10(String question10) {
        this.question10 = question10;
    }

    public Date getSubmitDatetime() {
        return submitDatetime;
    }

    public void setSubmitDatetime(Date submitDatetime) {
        this.submitDatetime = submitDatetime;
    }
}
