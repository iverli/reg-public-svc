package hk.org.ha.reg.pojo.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class RegdbUserDto {
    @Length(max = 255, message = "Name has been over length")
    @NotBlank(message = "required")
    private String userName;
    @Length(max = 50, message = "Phone number has been over length")
    @NotBlank(message = "required")
    @Pattern(regexp = "^(\\(\\d+\\))?\\d{8,}$", message = "Please enter correct phone number")
    private String phoneNumber;
    @Length(max = 255, message = "Email has been over length")
    @NotBlank(message = "required")
    @Pattern(regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$", message = "Please enter correct Email")
    private String email;
    @NotBlank(message = "required")
    @Length(max = 255, message = "Answer has been over length")
    private String question1;
    @NotBlank(message = "required")
    private String question2;
    @NotBlank(message = "required")
    @Length(max = 255, message = "Answer has been over length")
    private String question3;
    @NotBlank(message = "required")
    @Length(max = 255, message = "Answer has been over length")
    private String question4;
    @NotBlank(message = "required")
    @Length(max = 255, message = "Answer has been over length")
    private String question5;
    @Length(max = 255, message = "Answer has been over length")
    private String question6;
    @Length(max = 255, message = "Answer has been over length")
    private String question7;
    @Length(max = 255, message = "Answer has been over length")
    private String question8;
    @Length(max = 255, message = "Answer has been over length")
    private String question9;
    @Length(max = 255, message = "Answer has been over length")
    private String question10;

    @NotBlank(message = "required")
    private String token;

    public RegdbUserDto() {
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    public String getQuestion3() {
        return question3;
    }

    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    public String getQuestion4() {
        return question4;
    }

    public void setQuestion4(String question4) {
        this.question4 = question4;
    }

    public String getQuestion5() {
        return question5;
    }

    public void setQuestion5(String question5) {
        this.question5 = question5;
    }

    public String getQuestion6() {
        return question6;
    }

    public void setQuestion6(String question6) {
        this.question6 = question6;
    }

    public String getQuestion7() {
        return question7;
    }

    public void setQuestion7(String question7) {
        this.question7 = question7;
    }

    public String getQuestion8() {
        return question8;
    }

    public void setQuestion8(String question8) {
        this.question8 = question8;
    }

    public String getQuestion9() {
        return question9;
    }

    public void setQuestion9(String question9) {
        this.question9 = question9;
    }

    public String getQuestion10() {
        return question10;
    }

    public void setQuestion10(String question10) {
        this.question10 = question10;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
