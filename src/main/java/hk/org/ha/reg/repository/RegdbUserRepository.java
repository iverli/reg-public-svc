package hk.org.ha.reg.repository;

import hk.org.ha.reg.pojo.po.RegdbUserPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RegdbUserRepository extends JpaRepository<RegdbUserPo, Integer> {

    @Query(value = "select 1 from REGDB_USER where NAME = ?1 OR PHONE_NUMBER = ?2 OR EMAIL = ?3 limit 1",nativeQuery = true)
    List<Integer> findByNameAndEmailAndPhoneNumber(String userName, String phoneNumber, String email);

    @Override
    <S extends RegdbUserPo> S save(S entity);

}
