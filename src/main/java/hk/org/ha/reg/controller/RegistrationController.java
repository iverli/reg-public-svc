package hk.org.ha.reg.controller;

import hk.org.ha.reg.pojo.dto.RegdbUserDto;
import hk.org.ha.reg.response.Response;
import hk.org.ha.reg.service.CaptchaValidator;
import hk.org.ha.reg.service.RegService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
public class RegistrationController {

    private RegService regService;
    private CaptchaValidator captchaValidator;
    @Autowired
    public void setRegService(RegService regService) {
        this.regService = regService;
    }
    @Autowired
    public void setCaptchaValidator(CaptchaValidator captchaValidator) {
        this.captchaValidator = captchaValidator;
    }

    @CrossOrigin
    @PostMapping(value = "/registration", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Response registration(@RequestBody @Validated RegdbUserDto regdbUserDto, BindingResult result) {
        if (result.hasErrors()){
            return Response.result(202,result.getFieldError().getDefaultMessage());
        }
        captchaValidator.validateCaptcha(regdbUserDto.getToken());
        if(regService.registration(regdbUserDto)){
            return Response.success();
        }else {
            return Response.result(201,"Error, Exist Name/Phone Number/Email");
        }

    }
}
