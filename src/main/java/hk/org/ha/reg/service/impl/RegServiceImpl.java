package hk.org.ha.reg.service.impl;

import hk.org.ha.reg.pojo.dto.RegdbUserDto;
import hk.org.ha.reg.pojo.po.RegdbUserPo;
import hk.org.ha.reg.repository.RegdbUserRepository;
import hk.org.ha.reg.service.RegService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

import static hk.org.ha.reg.util.DtoMapUtil.convertRegdbUserPo;

@Service("regService")
public class RegServiceImpl implements RegService {

    private RegdbUserRepository regdbUserRepository;

    @Autowired
    public void setRegdbUserRepository(RegdbUserRepository regdbUserRepository) {
        this.regdbUserRepository = regdbUserRepository;
    }

    @Override
    public boolean registration(RegdbUserDto regdbUserDto) {

        RegdbUserPo regdbUserPo = convertRegdbUserPo(regdbUserDto);
        if (existUser(regdbUserPo)) {
            return false;
        }
        Date date = new Date();
        regdbUserPo.setSubmitDatetime(date);
        regdbUserRepository.save(regdbUserPo);
        return true;
    }

    public boolean existUser(RegdbUserPo regdbUserPo) {
        return regdbUserRepository.findByNameAndEmailAndPhoneNumber(regdbUserPo.getUserName(), regdbUserPo.getPhoneNumber(), regdbUserPo.getEmail()).size() == 1 ? true : false;
    }
}
