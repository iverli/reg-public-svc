package hk.org.ha.reg.service.impl;

import hk.org.ha.reg.response.CaptchaResponse;
import hk.org.ha.reg.exception.CaptchaErrorException;
import hk.org.ha.reg.service.CaptchaValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service("captchaValidator")
public class CaptchaValidatorImpl implements CaptchaValidator {

	private static final String GOOGLE_RECAPTCHA_ENDPOINT = "https://www.recaptcha.net/recaptcha/api/siteverify";

	@Value("${VIS_GOOGLE_RECAPTCHA_SEC}")
	private String recaptchaSecret;

	@Override
	public boolean validateCaptcha(String captchaResponse) {
		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<>();
		requestMap.add("secret", recaptchaSecret);
		requestMap.add("response", captchaResponse);

		CaptchaResponse apiResponse = restTemplate.postForObject(GOOGLE_RECAPTCHA_ENDPOINT, requestMap, CaptchaResponse.class);
		if (apiResponse != null && apiResponse.getSuccess() && apiResponse.getScore() != null && apiResponse.getScore().compareTo(0.5f) >= 0) {
			return true;
		}
		throw new CaptchaErrorException(apiResponse.getErrorCodes() == null ? "error" : apiResponse.getErrorCodes().toString());
	}

}