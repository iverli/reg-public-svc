package hk.org.ha.reg.service;

public interface CaptchaValidator {
    boolean validateCaptcha(String captchaResponse);
}
