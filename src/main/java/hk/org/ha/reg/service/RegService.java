package hk.org.ha.reg.service;

import hk.org.ha.reg.pojo.dto.RegdbUserDto;

public interface RegService {

    boolean registration(RegdbUserDto regdbUserDto);

}
